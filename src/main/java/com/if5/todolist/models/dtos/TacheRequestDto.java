package com.if5.todolist.models.dtos;

import java.util.Date;

import com.if5.todolist.models.entities.Tache;
import com.if5.todolist.models.entities.Utilisateur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TacheRequestDto {
	
	private Long id;

    private String libelle;

    private String description;
    
    private Date dateDebut;

    private Date dateFin;

    private Long utilisateur;

    private Long tacheParente;


    public static Tache builTacheFromDto(TacheRequestDto dto, Utilisateur utilisateur,Tache tacheParente ) {

        return Tache.builder()
                 .libelle(dto.getLibelle())
                 .description(dto.getDescription())
                 .dateDebut(dto.getDateDebut())
                 .dateFin(dto.getDateFin())
                 .utilisateur(utilisateur)
                 .tacheParente(dto.getTacheParente() == null ? null : tacheParente)
                .build();
        
    }

    /*public static TacheRequestDto builTacheDtoFromTache(Tache tache) {

        return TacheRequestDto.builder()
                 .libelle(tache.getLibelle())
                 .description(tache.getDescription())
                 .longitue(tache.getLongitue())
                 .latitude(tache.getLatitude())
                 .altitude(tache.getAltitude())
                 .lieuDeRappel(tache.getLieuDeRappel())
                 .dateDebut(tache.getDateDebut())
                 .dateFin(tache.getDateFin())
                 .dateCreation(tache.getDateCreation())
                 .dateModification(tache.getDateModification())
                 .utilisateur(tache.getUtilisateur().getId())
                 .tacheParente(tache.getTacheParente() == null ? null : tache.getTacheParente().getId())
                .build();
        
    }*/

    
}
