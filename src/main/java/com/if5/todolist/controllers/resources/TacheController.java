package com.if5.todolist.controllers.resources;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.if5.todolist.exceptions.EntityNotFoundException;
import com.if5.todolist.models.dtos.TacheRequestDto;
import com.if5.todolist.models.dtos.TacheResponseDto;
import com.if5.todolist.models.entities.Tache;
import com.if5.todolist.models.enumerations.StatutTache;
import com.if5.todolist.services.interfaces.TacheServiceInter;
import com.maxmind.geoip2.exception.GeoIp2Exception;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/toDoList/v1")
@Api("API pour la gestion des taches")
public class TacheController {


	@Autowired private TacheServiceInter tacheServiceInter;

	@ApiOperation(value = "get all task", notes = "récupere toutes les tâches dans la base de donnée", response = Tache.class)
	@ApiResponses(value= {
			@ApiResponse(code = 201, message ="les tâches  ont été récupéré avec succès") })
	@GetMapping("/taches")
	public ResponseEntity<List<TacheResponseDto>> getAllTache(){

		return ResponseEntity.ok(tacheServiceInter.getAllTask());
	}

	@ApiOperation(value = "create new task in database", notes = "elle permet la creation d'une nouvelle tâche  dans la base de donnée", response = Tache.class)
	@ApiResponses(value= {

			@ApiResponse(code = 201, message = "La tâche est creer avec succès"),
			@ApiResponse(code = 404, message = "La tâche fournie n'est pas  valide")
	})
	@PostMapping("/save-tache")
	public ResponseEntity<TacheResponseDto> saveTache(@RequestBody TacheRequestDto  tacheRequestDto) throws EntityNotFoundException{

		return new  ResponseEntity<TacheResponseDto>( tacheServiceInter.saveTask(tacheRequestDto), HttpStatus.CREATED);
	}

	@GetMapping("/tache-details/{id}")
	public ResponseEntity<TacheResponseDto> showDetailsOfAtask(@PathVariable long id) throws EntityNotFoundException{

		System.out.println(id);
		return ResponseEntity.ok(tacheServiceInter.showDetailsOfAtask(id));
	}

	@GetMapping("/tache-to-status")
	public ResponseEntity<List<TacheResponseDto>> taskAccordingToStatus(@RequestParam StatutTache status){
		System.out.println("********ok");
		return ResponseEntity.ok(tacheServiceInter.taskAccordingToStatus(status));
	}


	
		       @ApiResponses(value= {
		  
		            @ApiResponse(code = 201, message = "Le nombre de tâche à été calculer avec succès"),
		  
		            @ApiResponse(code = 404, message = "Les informations fournies ne sont pas correcte")
				 })
		  @PostMapping("/realise") 
		  public ResponseEntity<Integer> numberTache(@RequestBody TacheRequestDto tacheRequestDto){
			Integer numberOfTask = tacheServiceInter.nombreTache(tacheRequestDto);
		   return ResponseEntity.ok(numberOfTask);
		 }


		 @PutMapping("/ajout-coodonnees")
		 public ResponseEntity<Boolean> ajoutCoorGeoloc(
					@RequestParam(value = "libelle", required = true) String libelle, 
					@RequestParam(value ="ip", required = true) String ip) 
			                   throws EntityNotFoundException, GeoIp2Exception, IOException{
		  return new ResponseEntity<>(tacheServiceInter.ajoutCoordoneeGoe(libelle, ip ), HttpStatus.OK);
		 }
		 
		@PutMapping("/change-status/{id}")
		 public void  changeStatutTache(@PathVariable("id") Long id, @RequestParam String status ) throws EntityNotFoundException{
            
			StatutTache statut = StatutTache.valueOf(status);
		    tacheServiceInter.changeStatutTache(id, statut);

		 }
    

	@DeleteMapping("/tache/delete/{id}")
	public void deleteTask(@PathVariable Long id) throws EntityNotFoundException {
		tacheServiceInter.deleteTask(id);
	}
	

	@PostMapping("/heure-travaille/utilisateur")
	public ResponseEntity<Long> nombreHeure(@RequestBody  TacheRequestDto tacheRequestDto){
		Long nh = tacheServiceInter.nombreHeure(tacheRequestDto) ;
		return ResponseEntity.ok(nh);

	}



}



