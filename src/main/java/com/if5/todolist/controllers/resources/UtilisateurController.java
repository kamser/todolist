package com.if5.todolist.controllers.resources;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.if5.todolist.exceptions.DuplicationEntityException;
import com.if5.todolist.exceptions.EntityNotFoundException;
import com.if5.todolist.models.dtos.RoleRequestDto;
import com.if5.todolist.models.dtos.RoleResponseDto;
import com.if5.todolist.models.dtos.UtilisateurRequestDto;
import com.if5.todolist.models.dtos.UtilisateurResponseDto;
import com.if5.todolist.models.entities.Role;
import com.if5.todolist.models.entities.Utilisateur;
import com.if5.todolist.repositories.RoleRepository;
import com.if5.todolist.repositories.UtilisateurRepository;
import com.if5.todolist.services.interfaces.RoleServiceInter;
import com.if5.todolist.services.interfaces.UtilisateurServiceInter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/toDoList/v1")
@Api("API pour le CRUD des utilisateurs")
public class UtilisateurController {

   
	@Autowired private UtilisateurServiceInter utilisateurServiceInter;
	@Autowired private RoleServiceInter roleServiceInter;
    @Autowired private UtilisateurRepository utilisateurRepository;
    @Autowired private RoleRepository roleRepository;


    @PostMapping("/save-user")
    @ApiOperation(value = "Enregistrer un utilisateur",  response = Utilisateur.class)
    public ResponseEntity<UtilisateurResponseDto> saveUser(
    		                                @RequestBody UtilisateurRequestDto utilisateurRequestDto,
    		                                HttpServletRequest request
    		                                ) throws EntityNotFoundException, DuplicationEntityException,
                                                         UnsupportedEncodingException, MessagingException{
    	
      
        if(utilisateurRequestDto.getId() == null){
        	
            if(!utilisateurRequestDto.getPassword().equals(utilisateurRequestDto.getConfirmationPassword()) ||
            utilisateurRequestDto.getUserName() == null) { 
                         throw new EntityNotFoundException("mot de passe incorrect");
            }
                         Utilisateur user = utilisateurRepository.findByUserName(utilisateurRequestDto.getUserName());

           if(user!=null) {
                           throw new DuplicationEntityException("Ce nom d'utilisateur existe deja en base de donnée bien vouloir le changer");
               }

        }
        return new ResponseEntity<UtilisateurResponseDto>( utilisateurServiceInter.saveUser(utilisateurRequestDto,getSiteURL(request)),HttpStatus.CREATED);

    }

    @PostMapping("/save-role")
    @ApiOperation(value = "Enregistrer un role",  response = Role.class)
    public ResponseEntity<RoleResponseDto> saveRole(@RequestBody RoleRequestDto roleRequestDto) throws DuplicationEntityException{
    
        return ResponseEntity.ok(roleServiceInter.sauveRole(roleRequestDto));
    }
    

    @GetMapping("/users")
    @ApiOperation(value = "Lister tous les utilisateur")
    public ResponseEntity<List<UtilisateurResponseDto>> getAllUser(){
        return ResponseEntity.ok(utilisateurServiceInter.getAllUsers());
    }
 
    @GetMapping("/show-details/{userId}")
    @ApiOperation(value = "Affiche le detail d'un utilisateur")
    public ResponseEntity<UtilisateurResponseDto> showDetailsUser(@PathVariable Long userId) throws EntityNotFoundException{

        return ResponseEntity.ok(utilisateurServiceInter.showUserDetails(userId));
    }
    
    @PutMapping("/add-role")
    @ApiOperation(value = "Ajoute un role à un utilisateur")
    public ResponseEntity<String> addRoleToUser(@RequestParam String userName, @RequestParam String roleName) throws EntityNotFoundException{
       
    return new ResponseEntity<>(utilisateurServiceInter.addRoleToUser(userName, roleName), HttpStatus.CREATED);

    }
    
    @GetMapping("/roles")
    @ApiOperation(value = "Lister Tous les roles")
    public ResponseEntity<List<Role>> getRoles() {
        return ResponseEntity.ok(roleRepository.findAll());
    }
    
    
    private String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }  
    
    @GetMapping("/verify")
    public String verifyUser(@RequestParam("code") String code) {
    	System.out.println("*********Je suis appeler en cas d'activation de compte");
        if (utilisateurServiceInter.verify(code)) {
            return "Félicitation, votre compte est activé avec succes.";
        } else {
            return "Echec d'activation de compte.";
        }
    }
    
}
