package com.if5.todolist.services.interfaces;

import java.io.IOException;
import java.util.List;

import com.if5.todolist.exceptions.EntityNotFoundException;
import com.if5.todolist.models.dtos.TacheRequestDto;
import com.if5.todolist.models.dtos.TacheResponseDto;
import com.if5.todolist.models.enumerations.StatutTache;
import com.maxmind.geoip2.exception.GeoIp2Exception;

public interface TacheServiceInter {
    
    public TacheResponseDto saveTask(TacheRequestDto tacheRequestDto) throws EntityNotFoundException ;
	public List<TacheResponseDto> getAllTask();
    public boolean addTaskStatut(String taskname, StatutTache status); 
    public TacheResponseDto showDetailsOfAtask (Long id) throws EntityNotFoundException;
    public List<TacheResponseDto> taskAccordingToStatus( StatutTache status);
    public String deleteTask(Long id) throws EntityNotFoundException ;
    public long nombreHeure(TacheRequestDto tacheRequestDto);
    public Integer nombreTache(TacheRequestDto tacheRequestDto );
    public void changeStatutTache(Long id, StatutTache statut) throws EntityNotFoundException;
    public boolean ajoutCoordoneeGoe(String titre, String ip) throws EntityNotFoundException, GeoIp2Exception, IOException;
}
