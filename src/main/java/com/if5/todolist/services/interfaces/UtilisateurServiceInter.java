package com.if5.todolist.services.interfaces;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import com.if5.todolist.exceptions.EntityNotFoundException;
import com.if5.todolist.exceptions.InvalidEntityException;
import com.if5.todolist.models.dtos.UtilisateurRequestDto;
import com.if5.todolist.models.dtos.UtilisateurResponseDto;
import com.if5.todolist.models.entities.Utilisateur;

public interface UtilisateurServiceInter {

    public UtilisateurResponseDto saveUser(UtilisateurRequestDto utilisateurRequestDto, String siteURL)
    		        throws EntityNotFoundException, UnsupportedEncodingException, MessagingException;
	public List<UtilisateurResponseDto>  getAllUsers();
	public Utilisateur getUser(Long id) throws InvalidEntityException;
	public UtilisateurResponseDto updateUser(UtilisateurRequestDto utilisateurRequestDto) throws EntityNotFoundException;
	public UtilisateurResponseDto showUserDetails(Long userId) throws EntityNotFoundException;
	public String addRoleToUser(String userName,String roleName) throws EntityNotFoundException;
	public void sendVerificationEmail(Utilisateur Utilisateur, String siteURL)    	
			throws MessagingException, UnsupportedEncodingException ;
    public boolean verify(String verificationCode);
    
}
