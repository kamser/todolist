package com.if5.todolist.models.entities;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.if5.todolist.models.enumerations.StatutTache;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,  property="id")
public class Tache extends AuditModel {
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String libelle;

    private String description;

    private StatutTache statut;

    private double longitue;

    private double latitude;

    private String lieuDeRappel;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDebut;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFin;

    @ManyToOne
  //@JsonBackReference
    private Utilisateur utilisateur;

    @ManyToOne
   //@JsonBackReference
    private Tache tacheParente;

    @OneToMany(mappedBy = "tacheParente")
   //@JsonManagedReference
    private List<Tache> listSousTache = new ArrayList<>();

    @Override
    public String toString() {
      return "Tache [dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", description=" + description + ", id=" + id
          + ", latitude=" + latitude + ", libelle=" + libelle + ", lieuDeRappel=" + lieuDeRappel + ", longitue="
          + longitue + ", statut=" + statut + "]";
    }

}
