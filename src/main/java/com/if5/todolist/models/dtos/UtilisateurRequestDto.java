package com.if5.todolist.models.dtos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import com.if5.todolist.models.entities.Role;
import com.if5.todolist.models.entities.Tache;
import com.if5.todolist.models.entities.Utilisateur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UtilisateurRequestDto {

    private Long id;

   
    @NotNull
    @Column(unique = true)
    private String userName;

    private String nom;

    private String prenom;

    @Column(nullable = false, unique = true)
    private String email;

    private String password;

    private String confirmationPassword;

    private String villeDeResidence;

    private String paysOrigine;

    private String lieuDeNaissance;

    private LocalDate dateDeNaissance;

    private List<Long> roles;
    
   private List<Long> listTache;

    


    public static Utilisateur buildUserFromDto(UtilisateurRequestDto dto, List<Role> listRole, List<Tache> listTache){

        return Utilisateur.builder()
                     .userName(dto.getUserName())
                     .nom(dto.getNom())
                     .prenom(dto.getPrenom())
                     .email(dto.getEmail())
                     .password(dto.getPassword())
                     .villeDeResidence(dto.getVilleDeResidence())
                     .paysOrigine(dto.getPaysOrigine())
                     .lieuDeNaissance(dto.getLieuDeNaissance())
                     .dateDeNaissance(dto.getDateDeNaissance())
                     .roles(listRole == null ? new ArrayList<>() : listRole)
                     .listTache(listTache == null ? new ArrayList<>() : listTache)
                     .build();
    }
}
 