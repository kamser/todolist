package com.if5.todolist.repositories;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.if5.todolist.models.entities.Tache;
import com.if5.todolist.models.enumerations.StatutTache;

public interface TacheRepository  extends JpaRepository<Tache, Long>{

    public Optional<Tache> findByLibelle(String libelle);

   // public List<Tache> findAllByStatutTache(StatutTache status);
    
    //Premiere méthode
  // @Query(value="SELECT T FROM Tache T WHERE T.dateDebut BETWEEN :dateDebut AND :dateFin AND s.statut=com.if5.todolist.models.enumerations.StatutTache.REALISEE")
  //deuxieme méthode 
 // @Query(value="SELECT s FROM Tache s WHERE s.dateDebut<=:dateDebut AND s.dateFin<=:dateFin AND s.statut=com.if5.todolist.models.enumerations.StatutTache.REALISEE")  
  @Query(value="SELECT T FROM Tache T WHERE  T.dateDebut BETWEEN :dateDebut AND :dateFin AND T.dateFin BETWEEN :dateDebut AND :dateFin AND T.statut=:statut")
  public List<Tache> findAllByStatutTache(@Param("dateDebut") Date dateDebut, @Param("dateFin") Date dateFin);

   // @Query(value="SELECT a FROM Tache a WHERE a.dateDebut=:dateDebut AND a.dateFin<=:dateFin AND a.statut=com.if5.todolist.models.enumerations.StatutTache.REALISEE AND a.utilisateur.id=:utilisateur")
	@Query(value="SELECT a FROM Tache a WHERE a.dateDebut>=:dateDebut AND a.dateFin<=:dateFin AND a.utilisateur.id=:utilisateur AND a.statut=com.if5.todolist.models.enumerations.StatutTache.REALISEE")  
    public List<Tache> findAllTaches(@Param("dateDebut") Date startDate, @Param("dateFin") Date endDate, @Param("utilisateur") Long utilisateur);

    public Tache findByStatut(StatutTache status);

    @Query(value="SELECT a FROM Tache a WHERE a.statut=:status")
    public List<Tache> findAllByStatutTache(@Param("status") StatutTache status);
	
}
