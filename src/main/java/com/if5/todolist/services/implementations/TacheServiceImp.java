package com.if5.todolist.services.implementations;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.if5.todolist.exceptions.EntityNotFoundException;
import com.if5.todolist.models.dtos.TacheRequestDto;
import com.if5.todolist.models.dtos.TacheResponseDto;
import com.if5.todolist.models.entities.Tache;
import com.if5.todolist.models.entities.Utilisateur;
import com.if5.todolist.models.enumerations.StatutTache;
import com.if5.todolist.repositories.TacheRepository;
import com.if5.todolist.repositories.UtilisateurRepository;
import com.if5.todolist.services.interfaces.TacheServiceInter;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;

@Service
@Transactional
public class TacheServiceImp implements TacheServiceInter {

    private DatabaseReader dbReader;
   @Autowired private TacheRepository tacheRepository;
   @Autowired private UtilisateurRepository utilisateurRepository;


   public TacheServiceImp() throws IOException {
   
       File database = new File(System.getProperty("user.home") +"/GeoLite2/GeoLite2-City.mmdb" );
      
      
      dbReader = new DatabaseReader.Builder(database).build();
   }
   
@Override
public TacheResponseDto saveTask(TacheRequestDto tacheRequestDto) throws EntityNotFoundException {

    Tache tache = tacheRepository.findByLibelle(tacheRequestDto.getLibelle()).orElse(null);
     
    if( tache != null){
        throw new EntityNotFoundException("Cette tache existe deja dans la base de donnée");
    }

    Long tacheParentId = tacheRequestDto.getTacheParente() == null ? 0 :  tacheRequestDto.getTacheParente() ;
    Long userId = tacheRequestDto.getUtilisateur() == null ? 0 : tacheRequestDto.getUtilisateur();
   
    Tache tacheParente = tacheRepository.findById(tacheParentId).orElse(null);

    Utilisateur utilisateur = utilisateurRepository.findById(userId).orElse(null);
    
    if(tacheRequestDto.getDateDebut().after(tacheRequestDto.getDateFin())){

        throw new EntityNotFoundException("La date de debut de la tache ne peux pas etre situé apres la date de fin de la tache");
    }

    Tache t=TacheRequestDto.builTacheFromDto(tacheRequestDto, utilisateur, tacheParente);  
    		t.setStatut(StatutTache.EN_ATTENTE);
     Tache ts = tacheRepository.save(t);
          
    return TacheResponseDto.buildTacheFromDto(ts);
    
}

@Override
public List<TacheResponseDto> getAllTask() {

    return TacheResponseDto.buildListDtoFromListTache(tacheRepository.findAll());
}

@Override
public boolean addTaskStatut(String taskname, StatutTache status) {
    
    Tache tache = tacheRepository.findByStatut(status);
			         tache.setStatut(status);
			  
	 return true;
}

@Override
public TacheResponseDto showDetailsOfAtask(Long id) throws EntityNotFoundException {
    System.out.println(id);
		 Tache optionalTask = tacheRepository.findById(id).orElse(null);
	  System.out.println(optionalTask);
	  if(optionalTask == null) { 
		  throw new EntityNotFoundException("La tache avec l'ID " +id+ " n'existe pas en BDD"); 
	  }
	  return TacheResponseDto.buildTacheFromDto(optionalTask); 
	  }
	  

@Override
public List<TacheResponseDto> taskAccordingToStatus(StatutTache status) {
     List<Tache> listOfTask = tacheRepository.findAllByStatutTache(status);
   
  return  TacheResponseDto.buildListDtoFromListTache(listOfTask);
}

@Override
public String deleteTask(Long id) throws EntityNotFoundException {
    Tache tache = tacheRepository.getById(id);
		  
    if(!tache.getListSousTache().isEmpty()) {
        throw new EntityNotFoundException("Cette tache est lier une sous tache");
    }
        tacheRepository.delete(tache); 
        return "La tache à été supprimer avec succès";
}


@Override
public long nombreHeure(TacheRequestDto tacheRequestDto)  {
    
    List<Tache> tacheFromDB = tacheRepository.findAllTaches(tacheRequestDto.getDateDebut(), tacheRequestDto.getDateFin(), tacheRequestDto.getUtilisateur());

    long nombreHeure = 0;

  for(Tache tache : tacheFromDB){

    if(tache.getListSousTache() != null){

        for(Tache t : tache.getListSousTache()){

        long diff = t.getDateFin().getTime() - t.getDateDebut().getTime(); 
        
        TimeUnit time = TimeUnit.DAYS;
        long difference = time.convert(diff, TimeUnit.MILLISECONDS);

        nombreHeure += difference;
        } 
    }
    long diff = tache.getDateFin().getTime() - tache.getDateDebut().getTime(); 
        TimeUnit time = TimeUnit.DAYS;
        long difference = time.convert(diff, TimeUnit.MILLISECONDS);

        nombreHeure += difference;
    System.out.println("********************");
    System.out.println(tache.getLibelle() + " " +difference + " jours");
    System.out.println("********************");
  }
return nombreHeure;  
}


@Override
public Integer nombreTache(TacheRequestDto tacheRequestDto ) {

     Date dateDebut = tacheRequestDto.getDateDebut();
     Date dateFin = tacheRequestDto.getDateFin();
      List<Tache> tacheRealisee = tacheRepository.findAllByStatutTache(dateDebut,dateFin);
    
    return tacheRealisee.size();
}

@Override
public void changeStatutTache(Long id, StatutTache statut) throws EntityNotFoundException {

    Tache tache = tacheRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Cette tache n'existe pas dans la BD"));
	 
	  Date dateDebut = tache.getDateDebut();
	  
	  Date actualDate = new Date();
	  
	  Date dateFin = tache.getDateFin();
	  
	  if (dateDebut.before(actualDate) && statut == StatutTache.EN_COURS) {
	  
	      tache.setStatut(statut);
          tacheRepository.save(tache);
	  
	  }else if((dateDebut.before(actualDate)) && (statut == StatutTache.REALISEE)) {
	  
	  List<Tache> subTasks = (List<Tache>) tache.getListSousTache();
	  
	  int compteur = 0;
	  
	  for(Tache task: subTasks) {
	  
	  if(task.getStatut() != StatutTache.REALISEE) {
         compteur += compteur; }
         }
	 
	  if(compteur == 0 ) { tache.setStatut(statut);
        
	  tacheRepository.save(tache);
     }
     }else if((dateDebut.before(actualDate)) && (dateFin.after(actualDate)) && (statut == StatutTache.EN_COURS )) {
	  tache.setStatut(statut); 
      tacheRepository.save(tache);
      
	  }else if((dateFin.before(actualDate)) && tache.getStatut() != StatutTache.REALISEE) {
         tache.setStatut(StatutTache.EN_RETARD);
	  tacheRepository.save(tache); }
	  
	  else { tache.setStatut(StatutTache.ANNULE);
	 tacheRepository.save(tache); } 
    }


@Override
public boolean ajoutCoordoneeGoe(String titre, String ip) throws EntityNotFoundException, GeoIp2Exception, IOException {
     
    Tache tache = tacheRepository.findByLibelle(titre).orElse(null);

    if(tache == null){
        throw new EntityNotFoundException("Cette tache n'existe pas!");
    }
    
      InetAddress ipAddress = InetAddress.getByName(ip);
      CityResponse response = dbReader.city(ipAddress);
      
      String lieuDeRappel = response.getCity().getName();

      String latitude = response.getLocation().getLatitude().toString();
      Double lat =Double.parseDouble(latitude);

      String longitude = response.getLocation().getLongitude().toString();
      Double log =Double.parseDouble(longitude);

      tache.setLatitude(lat);
      tache.setLongitue(log);
      tache.setLieuDeRappel(lieuDeRappel);


      tacheRepository.save(tache);

      System.out.println(lieuDeRappel);


      return true;
  }


}


    

