package com.if5.todolist.exceptions.handlers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.if5.todolist.exceptions.DuplicationEntityException;
import com.if5.todolist.exceptions.EntityNotFoundException;
import com.if5.todolist.exceptions.InvalidEntityException;

@RestControllerAdvice
public class ToDoListHandler{

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String,String> handlerInvalidArgument(MethodArgumentNotValidException ex){
		 
		Map<String,String> errorsMap= new HashMap<>();
		ex.getBindingResult().getFieldErrors().forEach(error->{
			        errorsMap.put(error.getField(),error.getDefaultMessage());
		} );
		return errorsMap;
		
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String,String> handlerUserException(EntityNotFoundException ex){
		 
		Map<String,String> errorUserMap= new HashMap<>();
		errorUserMap.put("error", ex.getMessage());
		return errorUserMap;	
	}
	
	@ExceptionHandler(InvalidEntityException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String,String> handlerInvalidException(InvalidEntityException ex){
		 
		Map<String,String> errorUserMap= new HashMap<>();
		errorUserMap.put("error", ex.getMessage());
		return errorUserMap;	
	}

	
	@ExceptionHandler(DuplicationEntityException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String,String> handlerDuplicateException(DuplicationEntityException ex){
		 
		Map<String,String> errorUserMap= new HashMap<>();
		errorUserMap.put("error", ex.getMessage());
		return errorUserMap;	
	}
	

    
}
