package com.if5.todolist.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.if5.todolist.models.entities.Utilisateur;



@Transactional
@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long> {

    public Utilisateur findByUserName(String userName);
    public Utilisateur  findByEmail(String email);
    @Query("SELECT u FROM Utilisateur u WHERE u.verificationCode = ?1")
    public Utilisateur findByVerificationCode(String code);
    
    //public Utilisateur findByUserNameAndNomConteaints();
}
